package com.qrra.utils;

import com.qrra.pushNotification.model.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class NotificationLogBuilder {
    private List<NotificationLog> list;
    private Date publish_date = DateUtils.getSysDate();
    private String publish_time = DateUtils.getSysDateInFormat("HH:mm:ss.SSS");
    private String last_opr;
    private String appCode;
    private String function;
    private String status;
    private String message;
    private String store;
    private String title;
    private String content;
    private List<User> users;

    public NotificationLogBuilder(Date publish_date, String publish_time, String last_opr, String function, String status, String message, String title, String content) {
        this.publish_date = publish_date;
        this.publish_time = publish_time;
        this.last_opr = last_opr;
        this.function = function;
        this.status = status;
        this.message = message;
        this.title = title;
        this.content = content;
    }

    public NotificationLogBuilder() {

    }

    public NotificationLogBuilder(List<User> users) {
        this.users = users;
    }

    public NotificationLogBuilder setStore(String store) {
        this.store = store;
        return this;
    }

    public NotificationLogBuilder setappCode(String appCode) {
        this.appCode = appCode;
        return this;
    }

    public NotificationLogBuilder setMessage(String message) {
        this.message = message;
        return this;
    }

    public NotificationLogBuilder setTitle(String title) {
        this.title = title;
        return this;
    }

    public NotificationLogBuilder setContent(String content) {
        this.content = content;
        return this;
    }

    public NotificationLogBuilder setLast_opr(String last_opr) {
        this.last_opr = last_opr;
        return this;
    }

    public NotificationLogBuilder setFunction(String function) {
        this.function = function;
        return this;
    }

    public NotificationLogBuilder success(boolean yes) {
        this.status = yes ? "SUCCESS" : "FAIL";
        return this;
    }

    public List<NotificationLog> build() {
        List<NotificationLog> list = new ArrayList<>();

        int seq = 1;
        if (this.status == "SUCCESS") {
            for(User user : this.users) {
                NotificationLog log = new NotificationLog();
                log.setSeq(seq);
                log.setPublish_date(this.publish_date);
                log.setPublish_time(this.publish_time);
                log.setDevice_token(user.getDevice_token());
                log.setUser_id(user.getUser_id());
                log.setApp_code(this.appCode);
                log.setStore(this.store);
                log.setStatus(this.status);
                log.setMessage(null);
                log.setLast_opr(this.last_opr);
                log.setTitle(this.title);
                log.setFunction(this.function);
                log.setContent(this.content);
                log.setLast_opr_date(DateUtils.getSysDate().toString());
                log.setLast_version(DateUtils.getSysDateInMilli());

                list.add(log);
                seq ++;
            }
        } else {
            NotificationLog log = new NotificationLog();
            log.setSeq(seq);
            log.setPublish_date(this.publish_date);
            log.setPublish_time(this.publish_time);
            log.setDevice_token(null);
            log.setUser_id(null);
            log.setApp_code(this.appCode);
            log.setStore(this.store);
            log.setStatus(this.status);
            log.setMessage(this.message);
            log.setFunction(this.function);
            log.setLast_opr(this.last_opr);
            log.setTitle(this.title);
            log.setContent(this.content);
            log.setLast_opr_date(DateUtils.getSysDate().toString());
            log.setLast_version(DateUtils.getSysDateInMilli());

            list.add(log);
        }

        return list;
    }
}
