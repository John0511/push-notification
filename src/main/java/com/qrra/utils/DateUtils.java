package com.qrra.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {
    public static String sysDateFormat() {
        return "YYYY-MM-DD";
    }

    public static Date getSysDate() {
        Date dt = new Date();
        java.sql.Date sqlDt = new java.sql.Date(dt.getTime());

        return (parseSqlDate(sqlDt.toString(), DateUtils.sysDateFormat()));
    }

    public static String getSysDateInFormat(String format) {
        return new SimpleDateFormat(format).format(new Date());
    }

    public static long getSysDateInMilli() {
        Date dt = new Date();

        return dt.getTime();
    }

    public static java.sql.Date parseSqlDate(String str, String _format)

    {

        java.util.Date _dt = parseDate( str, _format);

        if (_dt == null)

            return (null);

        return (new java.sql.Date(_dt.getTime()));

    }

    public static Date parseDate(String str, String _format)

    {

        if (str == null || _format == null)

            return (null);

        if (str.length() < _format.length())

            return (null);

        Calendar caldr = Calendar.getInstance();

        caldr.clear();

        String YYYY = "";

        String MM = "";

        String DD = "";

        for (int i=0; i < _format.length(); i++)

        {

            if (Character.isDigit(str.charAt(i)) == false)

                continue;

            if (_format.charAt(i) == 'Y' || _format.charAt(i) == 'y')

                YYYY += str.charAt(i);

            else

            if (_format.charAt(i) == 'M' || _format.charAt(i) == 'm')

                MM += str.charAt(i);

            else

            if (_format.charAt(i) == 'D' || _format.charAt(i) == 'd')

                DD += str.charAt(i);

        }

        caldr.set(Integer.parseInt(YYYY), Integer.parseInt(MM)-1, Integer.parseInt(DD));

        return(caldr.getTime());

    }
}
