package com.qrra.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class EntityServiceBean<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<T> entity;

    public EntityServiceBean() {}

    public void setEntity(List<T> entity){
        this.entity = entity;
    }

    public List<T> getEntity(){
        if(entity == null){
            entity = new ArrayList<T>();
        }
        return entity;
    }


    @Override
    public String toString(){
        return "EntityServiceBean [entity=" + entity + "]";
    }
}
