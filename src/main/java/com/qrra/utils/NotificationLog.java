package com.qrra.utils;

import java.util.Date;

public class NotificationLog {
    private int seq;
    private Date publish_date;
    private String publish_time;
    private String device_token;
    private String user_id;
    private String app_code;
    private String store;
    private String status;
    private String message;
    private String title;
    private String content;
    private String function;
    private String last_opr;
    private String last_opr_date;
    private long last_version;

    public NotificationLog() {

    }

    public NotificationLog(int seq, Date publish_date, String publish_time, String device_token, String user_id, String app_code, String store, String status, String message, String title, String content, String function, String last_opr, String last_opr_date, long last_version) {
        this.seq = seq;
        this.publish_date = publish_date;
        this.publish_time = publish_time;
        this.device_token = device_token;
        this.user_id = user_id;
        this.app_code = app_code;
        this.store = store;
        this.status = status;
        this.message = message;
        this.title = title;
        this.content = content;
        this.function = function;
        this.last_opr = last_opr;
        this.last_opr_date = last_opr_date;
        this.last_version = last_version;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public Date getPublish_date() {
        return publish_date;
    }

    public void setPublish_date(Date publish_date) {
        this.publish_date = publish_date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPublish_time() {
        return publish_time;
    }

    public void setPublish_time(String publish_time) {
        this.publish_time = publish_time;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getApp_code() {
        return app_code;
    }

    public void setApp_code(String app_code) {
        this.app_code = app_code;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getLast_opr() {
        return last_opr;
    }

    public void setLast_opr(String last_opr) {
        this.last_opr = last_opr;
    }

    public String getLast_opr_date() {
        return last_opr_date;
    }

    public void setLast_opr_date(String last_opr_date) {
        this.last_opr_date = last_opr_date;
    }

    public long getLast_version() {
        return last_version;
    }

    public void setLast_version(long last_version) {
        this.last_version = last_version;
    }

    @Override
    public String toString() {
        return "NotificationLog{" +
                "seq=" + seq +
                ", publish_date=" + publish_date +
                ", publish_time='" + publish_time + '\'' +
                ", device_token='" + device_token + '\'' +
                ", user_id='" + user_id + '\'' +
                ", app_id='" + app_code + '\'' +
                ", store='" + store + '\'' +
                ", status='" + status + '\'' +
                ", message='" + message + '\'' +
                ", function='" + function + '\'' +
                ", last_opr='" + last_opr + '\'' +
                ", last_opr_date='" + last_opr_date + '\'' +
                ", last_version='" + last_version + '\'' +
                '}';
    }
}
