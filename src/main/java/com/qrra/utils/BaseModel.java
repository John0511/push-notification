package com.qrra.utils;

public abstract class BaseModel {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return new StringBuilder(512)
                .append("BaseModel[")
                .append("token=")
                .append(token)
                .append(']')
                .toString();
    }
}