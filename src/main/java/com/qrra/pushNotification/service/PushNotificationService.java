package com.qrra.pushNotification.service;

import com.qrra.pushNotification.model.NotificationData;
import com.qrra.pushNotification.model.PushNotificationRequest;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;
import com.qrac.agora.common.validation.Message;
import com.qrac.agora.common.validation.ValidationResult;
import com.qrra.pushNotification.model.User;
import com.qrra.utils.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.isBlank;

@Service
public class PushNotificationService implements DefaultService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PushNotificationService.class);

    private static String USR_REQ = "User list is required";
    private static String APP_CODE_REQ = "App code is required";
    private static String STORE_REQ = "Store is required";
    private static String TITLE_REQ = "Notification title is required";
    private static String CONTENT_REQ = "Notification content is required";

    private final String SERVICE_FILE_PATH = "google/service-account-file.json";
    private final String SEPERATOR = ",";

    private String errorMessage;

    private PushNotificationRequest pushRequest;

    private boolean sendResult;
    private boolean errorFlag;

    private ValidationResult<EmptyEntityResultBean> result;

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public PushNotificationService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public ValidationResult<EmptyEntityResultBean> send(EntityServiceBean<PushNotificationRequest> bean) {

        initAll();

        result = ValidationResult.<EmptyEntityResultBean>builder().build();

        if (bean.getEntity().isEmpty()) {
            result.addGlobalError(new Message("0", "Request body is empty"));
            errorFlag = true;
        } else {
            validateBean(bean.getEntity().get(0));
        }

        if (!errorFlag) {
            try {
                for (PushNotificationRequest request : bean.getEntity()) {
                    pushRequest = request;
                    sendResult = send();

                    if (!sendResult) {
                        errorFlag = true;
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                result.addGlobalError(new Message("0", ex.getMessage() == null ? ex.toString() : ex.getMessage()));
            }
        }

        return result;
    }

    private void initAll() {
        errorFlag = false;
        sendResult = false;
        pushRequest = null;
        errorMessage = null;
        result = null;
    }

    private boolean send() throws Exception {
        String appId = getAppId(pushRequest.getAppCode());
        if (!isNull(appId)) {
            List<User> userList = getDeviceToken(pushRequest.getUserList().stream().collect(Collectors.toList()), appId);
            if (!isNull(userList)) {
                LOGGER.info("**************** start publishing notification ****************");
                for (User user : userList) {
                    Notification notify = Notification.builder().setTitle(pushRequest.getNotification().getTitle()).setBody(pushRequest.getNotification().getContent()).build();
                    ClassLoader classLoader = getClass().getClassLoader();
                    InputStream inputStream = classLoader.getResourceAsStream(SERVICE_FILE_PATH);
                    FirebaseOptions options = FirebaseOptions.builder().setCredentials(GoogleCredentials.fromStream(inputStream)).build();
                    MulticastMessage message;

                    if (pushRequest.getNotification().getData() != null && pushRequest.getNotification().getData().size() > 0) {
                        Map<String, String> data = null;
                        for (NotificationData notificationData : pushRequest.getNotification().getData()) {
                            data.put(notificationData.getKey(), notificationData.getValue());
                        }
                        message = MulticastMessage.builder().putAllData(data).setNotification(notify).addToken(user.getDevice_token()).build();
                    } else {
                        message = MulticastMessage.builder().setNotification(notify).addToken(user.getDevice_token()).build();
                    }

                    FirebaseApp.initializeApp(options);
                    FirebaseMessaging.getInstance().sendMulticast(message);
                    FirebaseApp.getInstance().delete();
                }
                LOGGER.info("**************** end publishing notification ****************");

                //insert into notification log
                updateNotificationLog(userList, true);

            } else {
                result.addGlobalError(new Message("0", errorMessage));
                updateNotificationLog(null, false);
                return false;
            }
        } else {
            result.addGlobalError(new Message("0", errorMessage));
            updateNotificationLog(null, false);
            return false;
        }
        return true;
    }

    private void validateBean(PushNotificationRequest request) {
        try {
            if (isNull(request.getUserList()) || request.getUserList().size() == 0) {
                result.addGlobalError(new Message("0", USR_REQ));
                errorFlag = true;
            }
            if (isNull(request.getAppCode()) || isBlank(request.getAppCode())) {
                result.addGlobalError(new Message("0", APP_CODE_REQ));
                errorFlag = true;
            }
            if (isNull(request.getStore()) || isBlank(request.getStore())) {
                result.addGlobalError(new Message("0", STORE_REQ));
                errorFlag = true;
            }
            if (isNull(request.getNotification().getTitle()) || isBlank(request.getNotification().getTitle())) {
                result.addGlobalError(new Message("0", TITLE_REQ));
                errorFlag = true;
            }
            if (isNull(request.getNotification().getContent()) || isBlank(request.getNotification().getContent())) {
                result.addGlobalError(new Message("0", CONTENT_REQ));
                errorFlag = true;
            }
            if (StringUtils.isBlank(request.getAppCode())) {
                result.addGlobalError(new Message("0", APP_CODE_REQ));
                errorFlag = true;
            }
            if (request.getUserList().size() == 0) {
                result.addGlobalError(new Message("0", USR_REQ));
                errorFlag = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            result.addGlobalError(new Message("0", e.getMessage()));
            errorFlag = true;
        }
    }

    private boolean isNull(Object data) {
        return data == null ? true : false;
    }

    private List<User> getDeviceToken(List<String> userId, String appId) throws Exception {
        String users = "";
        List<User> tokenList;

        try {
            for (String user : userId) {
                if (users.length() > 0)
                    users += SEPERATOR + "'" + user + "'";
                else
                    users += "'" + user + "'";
            }
            if(userExist(users, appId)) {
                String query = " SELECT device_token, user_id FROM DEVICEMST " +
                        "WHERE STORE = '" + pushRequest.getStore() +
                        "' AND APP_ID = '" + appId +
                        "' AND USER_ID IN (" + users + ") ";
                tokenList = jdbcTemplate.query(query, BeanPropertyRowMapper.newInstance(User.class));
            } else {
                errorMessage = "User not found ("+users+")";
                return null;
            }
        } catch (DataAccessException e) {
            throw e;
        }

        return tokenList;
    }

    private Boolean userExist(String user, String appId) {
        String query = " SELECT count(*) FROM DEVICEMST " +
                "WHERE STORE = '" + pushRequest.getStore() +
                "' AND APP_ID = '" + appId +
                "' AND USER_ID IN (" + user + ") ";
        int count = jdbcTemplate.queryForObject(query, Integer.class);

        return count > 0 ? true : false;
    }

    private String getAppId(String appCode) throws Exception {
        String appId;
        try {
            if(appIdExist(appCode)) {
                String query = " SELECT app_id FROM APPMST WHERE APP_CODE = '" + appCode + "'";
                appId = jdbcTemplate.queryForObject(query, String.class);
            } else {
                errorMessage = "Application Code is not found in the system (" + appCode + ")";
                return null;
            }
        } catch (DataAccessException e) {
            throw e;
        }
        return appId;
    }

    private Boolean appIdExist(String appCode) {
        String query = " SELECT count(*) FROM APPMST WHERE APP_CODE = '" + appCode + "'";
        int count = jdbcTemplate.queryForObject(query, Integer.class);

        return count > 0 ? true : false;
    }

    private void updateNotificationLog(List<User> data, boolean isSuccess) {

        List<NotificationLog> record = new NotificationLogBuilder(data)
                .setFunction(pushRequest.getFunction())
                .setappCode(pushRequest.getAppCode())
                .setStore(pushRequest.getStore())
                .setMessage(errorMessage)
                .setTitle(pushRequest.getNotification().getTitle())
                .setContent(pushRequest.getNotification().getContent())
                .setLast_opr("JOHN")
                .success(isSuccess).build();

        try {
            jdbcTemplate.batchUpdate(
                    "INSERT INTO notification_log (seq, publish_date, publish_time, device_token, user_id, app_code, store, status, message, function, last_opr, last_opr_date, last_version, title, content) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                    new BatchPreparedStatementSetter() {

                        @Override
                        public void setValues(PreparedStatement ps, int i) throws SQLException {
                            ps.setInt(1, record.get(i).getSeq());
                            ps.setDate(2, Date.valueOf(record.get(i).getPublish_date().toString()));
                            ps.setString(3, record.get(i).getPublish_time());
                            ps.setString(4, record.get(i).getDevice_token());
                            ps.setString(5, record.get(i).getUser_id());
                            ps.setString(6, record.get(i).getApp_code());
                            ps.setString(7, record.get(i).getStore());
                            ps.setString(8, record.get(i).getStatus());
                            ps.setString(9, record.get(i).getMessage());
                            ps.setString(10, record.get(i).getFunction());
                            ps.setString(11, record.get(i).getLast_opr());
                            ps.setDate(12, Date.valueOf(record.get(i).getLast_opr_date()));
                            ps.setLong(13, record.get(i).getLast_version());
                            ps.setString(14, record.get(i).getTitle());
                            ps.setString(15, record.get(i).getContent());
                        }

                        @Override
                        public int getBatchSize() {
                            return record.size();
                        }
                    });
        } catch (Exception e) {
            throw e;
        }
    }
}
