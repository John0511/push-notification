package com.qrra.pushNotification.service;

import com.qrra.pushNotification.model.PushNotificationRequest;
import com.qrra.utils.EmptyEntityResultBean;
import com.qrra.utils.EntityServiceBean;
import com.qrac.agora.common.validation.ValidationResult;

public interface DefaultService {
    ValidationResult<EmptyEntityResultBean> send(EntityServiceBean<PushNotificationRequest> bean);
}
