package com.qrra.pushNotification.controller;

import com.qrra.pushNotification.model.PushNotificationRequest;
import com.qrra.pushNotification.service.PushNotificationService;
import com.qrra.utils.EmptyEntityResultBean;
import com.qrra.utils.EntityServiceBean;
import com.google.gson.Gson;
import com.qrac.agora.common.validation.StatusMessage;
import com.qrac.agora.common.validation.ValidationResult;
import com.qrac.agora.web.common.annotation.Path;
import com.qrac.agora.web.common.annotation.Post;
import com.qrac.agora.web.common.support.ResponseInfo;
import com.qrac.agora.web.common.support.StatusCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@Path("/")
public class PushNotificationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PushNotificationController.class);

    private final PushNotificationService pushNotificationService;

    @Autowired
    public PushNotificationController(PushNotificationService pushNotificationService) {
        this.pushNotificationService = pushNotificationService;
    }

    @Post("/publish")
    @ResponseBody
    public ResponseInfo<Void> search(@RequestBody EntityServiceBean<PushNotificationRequest> request)  {
        ValidationResult<EmptyEntityResultBean>  result = pushNotificationService.send(request);
        if (result == null || result.hasError()) {
            return ResponseInfo.<Void>builder()
                    .statusCode(StatusCode.ERROR)
                    .messages(StatusMessage.from(result))
                    .build();
        }
        return ResponseInfo.OK;
    }

}
