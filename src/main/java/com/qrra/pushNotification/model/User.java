package com.qrra.pushNotification.model;

public class User {
    private String device_token;
    private String user_id;

    public User() {
    }

    public User(String device_token, String user_id) {
        this.device_token = device_token;
        this.user_id = user_id;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
