package com.qrra.pushNotification.model;

import java.util.List;

public class PushNotificationRequest {
    private String store;
    private String appCode;
    private String userGroup;
    private List<String> userList;
    private Notification notification;
    private String function;

    public PushNotificationRequest() {

    }

    public PushNotificationRequest(String store, String appCode, String userGroup, List<String> userList, Notification notification, String function) {
        this.store = store;
        this.appCode = appCode;
        this.userGroup = userGroup;
        this.userList = userList;
        this.notification = notification;
        this.function = function;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public String getAppCode() {
        return appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    public List<String> getUserList() {
        return userList;
    }

    public void setUserList(List<String> userList) {
        this.userList = userList;
    }

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }

    public String getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(String userGroup) {
        this.userGroup = userGroup;
    }
}
