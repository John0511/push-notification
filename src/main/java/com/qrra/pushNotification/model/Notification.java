package com.qrra.pushNotification.model;

import java.util.List;

public class Notification {
    private String title;
    private String content;
    private List<NotificationData> data;

    public Notification() {

    }

    public Notification(String title, String content, List<NotificationData> data) {
        this.title = title;
        this.content = content;
        this.data = data;
    }

    public List<NotificationData> getData() {
        return data;
    }

    public void setData(List<NotificationData> data) {
        this.data = data;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
