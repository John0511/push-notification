package com.qrra;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@SpringBootApplication
public class PushNotificationApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(PushNotificationApplication.class);

    public static void main(String[] args) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        LOGGER.info("******** Application Start in "+ dtf.format(now) + " ****************");
        SpringApplication.run(PushNotificationApplication.class, args);
    }
}
